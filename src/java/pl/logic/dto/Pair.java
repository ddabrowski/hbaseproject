/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.logic.dto;

/**
 *
 * @author dawid
 */
public class Pair<E, F> {

    private E obj1;
    private F obj2;

    public Pair(E obj1, F obj2) {
        this.obj1 = obj1;
        this.obj2 = obj2;
    }

    public E getObj1() {
        return obj1;
    }

    public F getObj2() {
        return obj2;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Pair");
        sb.append("{obj1=").append(obj1);
        sb.append(", obj2=").append(obj2);
        sb.append('}');
        return sb.toString();
    }
}
