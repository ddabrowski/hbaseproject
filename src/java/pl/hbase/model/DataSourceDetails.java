/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.model;

import java.util.List;

/**
 *
 * @author dawid
 */
public class DataSourceDetails {
 
    private String dsn;
    private String ticker;
    private List<Double> values;

    public DataSourceDetails(String dsn, String ticker, List<Double> values) {
        this.dsn = dsn;
        this.ticker = ticker;
        this.values = values;
    }

    public String getDsn() {
        return dsn;
    }

    public void setDsn(String dsn) {
        this.dsn = dsn;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public List<Double> getValues() {
        return values;
    }

    public void setValues(List<Double> values) {
        this.values = values;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataSourceDetails other = (DataSourceDetails) obj;
        if ((this.dsn == null) ? (other.dsn != null) : !this.dsn.equals(other.dsn)) {
            return false;
        }
        if ((this.ticker == null) ? (other.ticker != null) : !this.ticker.equals(other.ticker)) {
            return false;
        }
        if (this.values != other.values && (this.values == null || !this.values.equals(other.values))) {
            return false;
        }
        return true;
    }
    
    
    
    
}
