/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.tools;

import java.util.UUID;

/**
 *
 * @author dawid
 */
public class IdGenerator {

    public static String id(){
        return UUID.randomUUID().toString();
    }
    
}
