<%@page language="Java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/includes.jsp" %>




<c:out value="${name}"/>


<script type="text/javascript">
   

   

    function actions(cellvalue, options, rowObject) {
      return  '<a class="delete warning" href="/dataSources/delete.htm?name=' + cellvalue + '">Usun</a> ';
    }

    function updateAction(cellvalue, options, rowObject) {
        return '<a class="update" href="/dataSources/updateData.htm?name='+cellvalue+'&url='+rowObject.url+'&type='+rowObject.sourceType+'">Pobierz dane</a>';
        
    }
    function initButtons() {
        $("a.delete").click(function (event) {
            if (!confirm('Chcesz trwale usunąć wpis?')) {
                event.preventDefault();
            }
        });

        $("a.update").click(function (event) {
            event.preventDefault();
            if (confirm('Chcesz uaktualnić te dane?')) {
                var location = $(this).attr('href');
                if (location != "#")
                    $.ajax({
                        type:'GET',
                        url:location,
                        cache:false,
                        timeout:240000,
                        success:function (data) {
                        },
                        error:function (data) {
                        }
                    });

                return true;
            }
        });
    }
    $(document).ready(function () {
        initButtons();

        $("#delDataSource-dialog-message").dialog({
            autoOpen:false,
            buttons:{
                Delete:function () {
                    var id = $(this).data('id');
                    $.ajax({
                        type:'POST',
                        url:'<c:url value="/admin/deleteDataSource.htm"/>',
                        data:{id:id},
                        cache:false,
                        timeout:240000,
                        success:function (data) {
                            //                            alert("data: "+data);
                            $("#dataSources").trigger('reloadGrid');
                        },
                        error:function () {
                            alert("Error while deleting date source.");
                        }
                    });
                    $(this).dialog("close");
                },
                Cancel:function () {
                    $(this).dialog("close");
                }
            }
        });


        $("#dataSources").jqGrid({
            autowidth:true,
            height:500,
            url:'/dataSources/detailsPage.htm?name=<c:out value="${name}"/>',
            datatype:"json",
            mtype:"GET",
              colNames:['Nazwa','Ticker', 'Wartości'],
            colModel:[
                {name:'dsn', index:'dsn', width:55, editable:false},
                {name:'ticker', index:'ticker', width:200, editable:false},
                {name:'values', index:'values', width:300, editable:false},
                
       
            ],
            multiselect:false,


            rowNum:30,
            rowList:[10, 20, 30, 50, 100, 1000],
            pager:'#dataSourcesPager',
            toppager:true,
            rownumbers:true,
            viewrecords:true,
            caption:"",
            gridComplete:function (id) {
                $(".delDataSource").bind('click', function (event) {
                    event.preventDefault();
                    var id = $(this).attr('href');
                    $("#delDataSource-dialog-message").data('id', id).dialog('open');
                });

               

                $(".button,.smallButton,.tinyButton").button();
                initButtons();
            },

            jsonReader:{
                root:"rows",
                page:"page",
                total:"total",
                records:"records",
                repeatitems:false,
                userdata:"userdata"
            }
        });

        $("#dataSources").jqGrid('navGrid', '#dataSourcesPager', {search:false, edit:false, add:false, del:false});
    });
</script>


<br/>
<br/>
<%--@elvariable id="dataSources" type="java.util.List<pl.benhauer.inwestor.model.data.DataSource>"--%>
<table id="dataSources"></table>
<div id="dataSourcesPager"></div>

<div class="hidden">
    <div id="delDataSource-dialog-message" title="Data Source has been deleted">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
          delete
        </p>
    </div>
</div>