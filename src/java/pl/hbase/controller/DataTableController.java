/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.hbase.dao.IDataTableDao;
import pl.hbase.dto.DataPageResponse;
import pl.hbase.facade.IFacade;
import pl.hbase.model.DataSource;
import pl.hbase.model.DataSourceDetails;
import pl.hbase.tools.FileTools;
import pl.hbase.tools.UrlTools;
import pl.logic.dto.Pair;

/**
 *
 * @author dawid
 */
@Controller
public class DataTableController {
    
    private IFacade facade;
    private IDataTableDao dataTableDao;
    
    @Autowired
    public DataTableController(IFacade facade, IDataTableDao dataTableDao){
        this.facade = facade;
        this.dataTableDao = dataTableDao;
    }
    
      @RequestMapping("/dataSources/updateData.htm")
    public String downloadTest(Model model, 
                                                    @RequestParam(value="name")String name,
                                                    @RequestParam(value="url")String url,
                                                    @RequestParam(value="type", defaultValue = "txt")String type) throws FileNotFoundException{
        
        
        if("TXT".equals(type)){
            facade.updateDataSource(url, name, DataSource.SourceType.TXT);
        }

        if("ZIP".equals(type)){
            
            facade.updateDataSource(url, name, DataSource.SourceType.ZIP);
        }
       
        
        return "redirect:/dataSources/list.htm";
    }
    
      @RequestMapping("/dataSources/showDetails.htm")
      public String showDetails(Model model, @RequestParam(value="name")String name){
          model.addAttribute("name", name);
          return "dataDetailsView";
      }
      
      
      @RequestMapping("/dataSources/detailsPage.htm")
      public 
            @ResponseBody
            DataPageResponse<DataSourceDetails> list (
                                                   @RequestParam(value="name")String name,
                                                   @RequestParam(value="page", required= false, defaultValue = "1")int page,
                                                   @RequestParam(value="rows", required= false, defaultValue = "10")int rows){
        
            return dataTableDao.findDataPages(name, page, rows);
    }
      
    @RequestMapping("/dataSources/chartData.htm")
    public 
            @ResponseBody
            List<Double>  chart (@RequestParam(value="name")String name){
        
            List<Double> chartList = new ArrayList<Double>();
               
            List<DataSourceDetails> dsdList = dataTableDao.findData(name);
            
            for(DataSourceDetails dsd : dsdList){
                chartList.add(dsd.getValues().get(1));
            }
            
            
            
            return chartList;
    }  
      
      
    @RequestMapping("/dataSources/showChart.htm")
    public String showChart(Model model, @RequestParam(value="name")String name){
        model.addAttribute("name", name);
        return "chartView";
    }
}
    
    

