<%@page language="Java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/includes.jsp" %>

<script type="text/javascript">    

$(document).ready(function(){
  // Our data renderer function, returns an array of the form:
  // [[[x1, sin(x1)], [x2, sin(x2)], ...]]
  
  
  
//  /dataSources/chartData.htm?name=mstock2
  
  
  var txt;

$.ajax({
    url: '/dataSources/chartData.htm?name=<c:out value="${name}"/>',
    dataType: 'text',
    async: false,
    success: function(data) {
        partnerData = data;
    }
});

  var partsOfStr = partnerData.split(",");


  var sineRenderer = function() {
    var data = [[]];
    for (var i=1; i<partsOfStr.length--; i++) {
      data[0].push(parseFloat(partsOfStr[i]));
      
    }
    return data;
  };

  var plot1 = $.jqplot('chart1',[],{
      title: 'Wykres wartości',
      dataRenderer: sineRenderer
  });
  
});



</script>


<div id="chart1" style="height:600px; width:880px;"></div>
<div class="code prettyprint">
<pre class="code prettyprint brush: js"></pre>
</div>

<div  id="dane"></div>