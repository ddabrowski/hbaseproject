/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.dao;

import java.io.UnsupportedEncodingException;
import java.nio.charset.CharacterCodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.ParseFilter;
import org.apache.hadoop.hbase.filter.RegexStringComparator;
import org.apache.hadoop.hbase.filter.ValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.data.hadoop.hbase.HbaseTemplate;
import org.springframework.data.hadoop.hbase.ResultsExtractor;
import org.springframework.data.hadoop.hbase.RowMapper;
import org.springframework.data.hadoop.hbase.TableCallback;
import pl.hbase.dto.DataPageResponse;
import pl.hbase.dto.ItemsPage;
import pl.hbase.model.DataSource;
import pl.hbase.model.DataSourceDetails;
import pl.hbase.tools.DateTools;
import pl.hbase.tools.FileTools;
import pl.hbase.tools.IdGenerator;

/**
 *
 * @author dawid
 */
public class DataTableDao implements IDataTableDao {
        
    private HbaseTemplate hbaseTemplate;
    
    private static final Logger LOG = Logger.getLogger(DataSourceDao.class.getName());
    
    private static final String TABLE_NAME = "datatable";
    private static final byte[] COL_FAM = Bytes.toBytes("data");
    private static final byte[] DATA_SOURCE_NAME = Bytes.toBytes("dsn");
    private static final byte[] TICKER = Bytes.toBytes("ticker");
    private static final int idLength = 36;
    private static final int hashLength = 16;

    public DataTableDao(HbaseTemplate hbaseTemplate) {
        this.hbaseTemplate = hbaseTemplate;
    }
    
    
    
    /**
     *
     * @param data
     * @param name
     */
    @Override
    public void addDataTable(final String line, final String name){
        hbaseTemplate.execute(TABLE_NAME, new TableCallback<Put>() {

            @Override
            public Put doInTable(HTableInterface hti) throws Throwable {
                
                
                String id = IdGenerator.id();
                
                byte[] idBytes = Bytes.toBytes(id);
                
                byte[] userHash = MD5bytes(name);
                
                byte[] rowKey = new byte[idLength + userHash.length];
                        
                int offset = 0;
                
                offset = Bytes.putBytes(rowKey, offset, userHash, 0, userHash.length);
                Bytes.putBytes(rowKey, offset, idBytes, 0, idBytes.length);
                
                Put put = new Put(rowKey);
                
                put.add(COL_FAM, DATA_SOURCE_NAME, Bytes.toBytes(name));
                System.out.println(name);
                
                String[] data = FileTools.getEntries(line);
                
                put.add(COL_FAM, TICKER, Bytes.toBytes(data[0]));
                put.add(COL_FAM, Bytes.toBytes("timestamp"), Bytes.toBytes(String.valueOf((long)(-1*DateTools.getTimestamp()))));
                
                System.out.println(data[0]);
                for(int i =1; i < data.length; i++){
                    put.add(COL_FAM, Bytes.toBytes((int)i), Bytes.toBytes(Double.parseDouble(data[i])));
                    System.out.println(data[i]);
                }
                
                hti.put(put);
                
                return put; 
            }
        });
    }
    

    
    @Override
    public List<DataSourceDetails> findData(String name, int page, int rows){
        byte[] userHash = MD5bytes(name);
        byte[] startRow = Bytes.padTail(userHash,idLength ); // 212d...866f00...
        byte[] stopRow = Bytes.padTail(userHash, idLength) ;
        
        stopRow[hashLength-1]++;                      // 212d...867000...
        
        Scan s = new Scan(startRow, stopRow);
        
        List<DataSourceDetails> dataSourceDetailses =
        hbaseTemplate.find(TABLE_NAME, s, new RowMapper<DataSourceDetails>() {

            @Override
            public DataSourceDetails mapRow(Result result, int i) throws Exception {
                
                List<Double> values;
                values = new ArrayList<Double>();
                
                int a = 1; 
                
                
                double temp;
                
                while(result.containsColumn(COL_FAM, Bytes.toBytes(a))){
                    temp = Bytes.toDouble(result.getValue(COL_FAM, Bytes.toBytes(a)));
                    values.add(temp);
                    a++;
                    
                }
                
                String dsn = Bytes.toString(result.getValue(COL_FAM, DATA_SOURCE_NAME));
                String ticker = Bytes.toString(result.getValue(COL_FAM, TICKER));
                
                return new DataSourceDetails(dsn, ticker, values);
            }
        });
    
        return dataSourceDetailses;

    }
    
    @Override
    public List<DataSourceDetails> findData(String name){
        byte[] userHash = MD5bytes(name);
        byte[] startRow = Bytes.padTail(userHash,idLength ); // 212d...866f00...
        byte[] stopRow = Bytes.padTail(userHash, idLength) ;
        
        stopRow[hashLength-1]++;                      // 212d...867000...
        
        Scan s = new Scan(startRow, stopRow);
        
        List<DataSourceDetails> dataSourceDetailses =
        hbaseTemplate.find(TABLE_NAME, s, new RowMapper<DataSourceDetails>() {

            @Override
            public DataSourceDetails mapRow(Result result, int i) throws Exception {
                
                List<Double> values;
                values = new ArrayList<Double>();
                
                int a = 1; 
                
                
                double temp;
                
                while(result.containsColumn(COL_FAM, Bytes.toBytes(a))){
                    temp = Bytes.toDouble(result.getValue(COL_FAM, Bytes.toBytes(a)));
                    values.add(temp);
                    a++;
                    
                }
                
                String dsn = Bytes.toString(result.getValue(COL_FAM, DATA_SOURCE_NAME));
                String ticker = Bytes.toString(result.getValue(COL_FAM, TICKER));
                
                return new DataSourceDetails(dsn, ticker, values);
            }
        });
    
        return dataSourceDetailses;

    }
    
    
    @Override
    public DataPageResponse<DataSourceDetails> findDataPages (String name, int page, int rows){
        ItemsPage<DataSourceDetails> itemsPage = new ItemsPage<DataSourceDetails>(page, rows);
        itemsPage.setItems(findData(name, page, rows));
        return new DataPageResponse<DataSourceDetails>(itemsPage);
    }
    
    public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException{
        String id = IdGenerator.id();
        byte[] idbyte = id.getBytes();
        
        System.out.println(idbyte.length);
//        System.out.println(idbyte+" "+Bytes.toString(idbyte));
//        idbyte = Bytes.toBytes(id);
//        System.out.println(idbyte+" "+Bytes.toString(idbyte));
//        System.out.println(id.getBytes().length+" "+Bytes.toBytes(id).length);
//        byte[] timestamp = Bytes.toBytes((long)(-1*DateTools.getTimestamp()));
//        System.out.println(timestamp.toString()+" po ludzku "
//                + " "+Bytes.toLong(timestamp));
        
                
        String string = "name";
        byte[] bytesOfMessage = string.getBytes("UTF-8");

        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] thedigest = md.digest(bytesOfMessage);
        
        System.out.println(MD5("name"));
        byte[] userHash = MD5bytes("name");
        System.out.println(userHash.length);
    }
    
    
   public static String MD5(String md5) {
   try {
        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
        byte[] array = md.digest(md5.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
       }
        return sb.toString();
    } catch (java.security.NoSuchAlgorithmException e) {
    }
    return null;
}
   
   
   public static byte[] MD5bytes(String md5){
        try {
             MessageDigest md = java.security.MessageDigest.getInstance("MD5");
             byte[] array = md.digest(md5.getBytes());
             return array;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DataTableDao.class.getName()).log(Level.SEVERE, 
                    null, ex);
        }
       
        return null;
   }
   
    /*int longLength = Long.SIZE / 8;
byte[] userHash = Md5Utils.md5sum("TheRealMT");
byte[] timestamp = Bytes.toBytes(-1 * 1329088818321L);
byte[] rowKey = new byte[Md5Utils.MD5_LENGTH + longLength];
int offset = 0;
offset = Bytes.putBytes(rowKey, offset, userHash, 0, userHash.length);
Bytes.putBytes(rowKey, offset, timestamp, 0, timestamp.length);
*/
   
/*
 * byte[] userHash = Md5Utils.md5sum(user);
byte[] startRow = Bytes.padTail(userHash, longLength); // 212d...866f00...
byte[] stopRow = Bytes.padTail(userHash, longLength);
stopRow[Md5Utils.MD5_LENGTH-1]++;                      // 212d...867000...
Scan s = new Scan(startRow, stopRow);
ResultsScanner rs = twits.getScanner(s);

* for(Result r : rs) {
  // extract the username
  byte[] b = r.getValue(
    Bytes.toBytes("twits"),
    Bytes.toBytes("user"));
  String user = Bytes.toString(b);
  // extract the twit
  b = r.getValue(
    Bytes.toBytes("twits"),
    Bytes.toBytes("twit"));
  String message = Bytes.toString(b);
  // extract the timestamp
  b = Arrays.copyOfRange(
    r.getRow(),
    Md5Utils.MD5_LENGTH,
    Md5Utils.MD5_LENGTH + longLength);
  DateTime dt = new DateTime(-1 * Bytes.toLong(b));
}

 */
    
}
/*
 * regex comparator "
 * Scan s = new Scan();
s.addColumn(Bytes.toBytes("twits"), Bytes.toByes("twit"));
Filter f = new ValueFilter(
  CompareOp.EQUAL,
  new RegexStringComparator(".*TwitBase.*"));
s.setFilter(f)
------------------
* 
* Scan s = new Scan();
s.addColumn(TWITS_FAM, TWIT_COL);
String expression = "ValueFilter(=,'regexString:.*TwitBase.*')";
ParseFilter p = new ParseFilter();
Filter f = p.parseSimpleFilterExpression(Bytes.toBytes(expression));
s.setFilter(f);

 */