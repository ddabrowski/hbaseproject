<%@page language="Java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/includes.jsp" %>



<form:form method="post" modelAttribute="dataSourceForm" id="addDataSource">
        <h1>Dodaj źródło danych</h1>

        <p> </p>

        
        <table>
            <tr>
        
        
        <%--@declare id="email"--%>
        <td class="left"><label for="name">Nazwa: </label></td>
        <td>
        <form:input path="name"/>
        <form:errors path="name" cssClass="errors">
            <div class="errors">
                <c:forEach items="${messages}" var="msg">
                    <div class="error"><spring:message code="${msg}" text="${msg}"/></div>
                </c:forEach>
            </div>
        </form:errors><br/>
        </td>
        
        </tr>
        
        <br/>
        <tr>
        
            <td class="left" ><label for="url">Url: </label></td>
            <td>
        <form:input path="url" id="url"/>
        <form:errors path="url" cssClass="errors">
            <div class="errors">
                <c:forEach items="${messages}" var="msg">
                    <div class="error"><spring:message code="${msg}" text="${msg}"/></div>
                </c:forEach>
            </div>
        </form:errors><br/>
            </td>
        </tr>

        
        
</table>
        <br/>
        <div class="formLine">
        <label for="role">Typ pliku: </label>
        <form:select id="sourceType" path="sourceType" title="sourceType">

            <%--@elvariable id="categories" type="java.util.List"--%>
            <form:options items="${types}" itemValue="obj1" itemLabel="obj2"/>
        </form:select>
        <form:errors path="sourceType" cssClass="errors">
            <div class="errors">
                <c:forEach items="${messages}" var="msg">
                    <div class="error"><spring:message code="${msg}" text="${msg}"/></div>
                </c:forEach>
            </div>
        </form:errors><br/>
     </div>

        
        <div class="buttonsWrapper">
        <a class="button-style" class="button" href="<c:url value="/"/>">Anuluj</a>
            <input  class="button button-style" type="submit" value="Dodaj"/>
    </div>
        
        
    </form:form>
