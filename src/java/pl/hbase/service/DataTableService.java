/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.service;

import pl.hbase.dao.IDataTableDao;

/**
 *
 * @author dawid
 */
public class DataTableService implements IDataTableService{
    
    private IDataTableDao dataTableDao;

    public DataTableService(IDataTableDao dataTableDao) {
        this.dataTableDao = dataTableDao;
    }
    
    @Override
    public void putLine(String line, String name){
        dataTableDao.addDataTable(line, name);
    }
    
    
}
