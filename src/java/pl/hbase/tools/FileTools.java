/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.tools;

/**
 *
 * @author dawid
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.hadoop.hbase.util.Bytes;
import pl.hbase.facade.Facade;

public class FileTools {
    
    private static final Logger log = Logger.getLogger(FileTools.class.getName());

 
    public static List<File> unZipFile(File tempFile) {
        List<File> fileList = new ArrayList<File>();
        byte[] buffer = new byte[1024];
        try {
            File folder = tempFile.getParentFile();

            log.log(Level.INFO, "Unpacking path:{0}", folder.getAbsolutePath());
            ZipInputStream zis = new ZipInputStream(new FileInputStream(tempFile));
            ZipEntry ze = zis.getNextEntry();
            File tmp;
            while (ze != null) {
                String fileName = ze.getName();
                tmp = File.createTempFile("inw_", "_" + fileName);
                log.log(Level.INFO, "File unzip : {0}", tmp.getAbsoluteFile());
                FileOutputStream fos = new FileOutputStream(tmp);
                int len;
                while ((len = zis.read(buffer)) > 0)
                    fos.write(buffer, 0, len);

                fos.close();
                fileList.add(tmp);
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return fileList;

    }
    
    public static void deleteTempFile(File file) {
        if (!file.delete())
            log.log(Level.INFO, "Cannot delete file{0}", file.getAbsolutePath());
    }
    
    public static String getTextFromFile(File file) throws FileNotFoundException{
        Scanner in = new Scanner(file);
        String text = "";
        while(in.hasNextLine()){
            text+=in.nextLine()+";";
            
            
        }
        return text;
    }
    
    public static String[] getLines(String text){
        String[] lines = text.split(";");
        return lines;
    }
    
    public static String[] getEntries(String line){
        String[] entries = line.split(",");
        return entries;
    }
    
    public static void main(String[] args){
        
        String url="http://bossa.pl/pub/metastock/mstock/sesjaall/20130510.prn";
        
        try {
                File file = UrlTools.downloadFile(url);
                
                        System.out.println(file.toString());
                      
                String text = FileTools.getTextFromFile(file);
                        System.out.println(url);
                        System.out.println("Tekst " + text);
               
                int i =0 ;
               
                for(String s : getLines(text)){
                    i++;
                    if(i<5){
                        //dataTableService.putLine(s, name);
                        System.out.println("Name " + "S "+s);
                    }
                   
                }
               
                file.delete();
                
                    
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Facade.class.getName()).log(Level.SEVERE, null, ex);
            } 
    
    }
    
}
         
         
