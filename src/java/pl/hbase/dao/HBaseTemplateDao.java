/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.dao;

import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.data.hadoop.hbase.HbaseTemplate;
import org.springframework.data.hadoop.hbase.TableCallback;

/**
 *
 * @author dawid
 */
public class HBaseTemplateDao implements IHBaseTemplateDao{
    
    HbaseTemplate hbaseTemplate;

    public HBaseTemplateDao(HbaseTemplate hbaseTemplate) {
        this.hbaseTemplate = hbaseTemplate;
    }
    
    
    public Object putToHBaseTest(final String text){
        
       hbaseTemplate.execute("test", new TableCallback<Object>() {


           @Override
           public Object doInTable(HTableInterface hti) throws Throwable {
               Put p = new Put(Bytes.toBytes("row1"));
               p.add(Bytes.toBytes("cf"), Bytes.toBytes(text), Bytes.toBytes("value1"));
               hti.put(p);
               return null;
           }
       });
        
        return null;
    }

    
    /* hbaseTemplate.execute('test', new TableCallback<Object>() {
            @Override
            public Object doInTable(HTable table) throws Throwable {
                 Put p = new Put(Bytes.toBytes(text));
                 p.add(Bytes.toBytes("SomeColumn"), Bytes.toBytes("SomeQualifier"), Bytes.toBytes("AValue"));
                 table.put(p);
                return null;
           }
          
        });*/
}
