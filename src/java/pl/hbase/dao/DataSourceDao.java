/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.PageFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.data.hadoop.hbase.HbaseTemplate;
import org.springframework.data.hadoop.hbase.ResultsExtractor;
import org.springframework.data.hadoop.hbase.RowMapper;
import org.springframework.data.hadoop.hbase.TableCallback;
import pl.hbase.model.DataSource;

/**
 *
 * @author dawid
 */
public class DataSourceDao implements IHBaseTemplateDao, IDataSourceDao{

    private HbaseTemplate hbaseTemplate;
    
    private static final Logger LOG = Logger.getLogger(DataSourceDao.class.getName());
    
    private static final String TABLE_NAME = "datasources";
    private static final byte[] COL_FAM = Bytes.toBytes("data");
    private static final byte[] NAME_COL = Bytes.toBytes("name");
    private static final byte[] URL_COL = Bytes.toBytes("url");
    private static final byte[] SOURCE_TYPE_COL = Bytes.toBytes("sourcetype");
    private List<DataSource> dataSources;

    
    
    public DataSourceDao(HbaseTemplate hbaseTemplate) {
        this.hbaseTemplate = hbaseTemplate;
    }

    
    
    @Override
    public DataSource addDataSource(final DataSource dataSource){
        hbaseTemplate.execute(TABLE_NAME, new TableCallback<Put>() {

            @Override
            public Put doInTable(HTableInterface hti) throws Throwable {
                Put p = new Put(Bytes.toBytes(dataSource.getName()));
                p.add(COL_FAM, NAME_COL, Bytes.toBytes(dataSource.getName()));
                p.add(COL_FAM, URL_COL, Bytes.toBytes(dataSource.getUrl()));
                p.add(COL_FAM, SOURCE_TYPE_COL, Bytes.toBytes(dataSource.getSourceType().name()));
                hti.put(p);
                return p;
            }
        });
                
        return dataSource;
    }
    
    @Override
    public List<DataSource> findAll() {
        return hbaseTemplate.find(TABLE_NAME,"data", new RowMapper<DataSource>() {
            @Override
            public DataSource mapRow(Result result, int i) throws Exception {
                return new DataSource(Bytes.toString(result.getRow()),
                        Bytes.toString(result.getValue(COL_FAM, URL_COL)),
                        DataSource.SourceType.valueOf((Bytes.toString(result.getValue(COL_FAM, SOURCE_TYPE_COL))))
                        );
            }
        });

}
    
    
    @Override
    public void deleteDataSource(final DataSource dataSource){
        
                
        hbaseTemplate.execute(TABLE_NAME, new TableCallback<Delete>(){
            @Override
            public Delete doInTable(HTableInterface hti) throws Throwable {
                Delete delete = new Delete(Bytes.toBytes(dataSource.getName()));
                delete.deleteFamily(COL_FAM);
                
                hti.delete(delete);
                LOG.info("nazwa wiersza"+dataSource.getName());
                return delete;
            }
            
            
        
        });
        
    }
    
    
    @Override
    public List<DataSource> findDataSources(int page, final int rows){
        
        dataSources = new ArrayList<DataSource>();
        byte[] startRow;
        
        Scan scan = new Scan(); 
        
          if(page == 1){
              hbaseTemplate.execute(TABLE_NAME, new TableCallback<Scan>() {

                  @Override
                  public Scan doInTable(HTableInterface hti) throws Throwable {
                      Filter filter = new PageFilter((long)rows);
                      Scan scan = new Scan();
                      scan.setFilter(filter);
                      
                      ResultScanner rs = hti.getScanner(scan);
                      Result result;
                     
                      while((result = rs.next())  != null){
                         dataSources.add(mapDataSource(result));
                      }
                      rs.close();
                      return scan;
                  }
              });
              
              return dataSources;
          }      
                
          
          if(page > 1){
              final long  firstRowNumber = (page-1)*rows+1;
              Filter filter = new PageFilter(firstRowNumber);
              scan = new Scan();
              scan.setFilter(filter);
              
              DataSource dataSource;
              dataSource = hbaseTemplate.find(TABLE_NAME, scan, new ResultsExtractor<DataSource>() {
                  @Override
                  public DataSource extractData(ResultScanner rs) throws Exception {
                      Result result;
                      DataSource dataSource = new DataSource();
                      while((result =rs.next())!=null){
                            dataSource = mapDataSource(result);
                      }
                      return dataSource;
                      
                  }
              });
              
              startRow = Bytes.toBytes(dataSource.getName());
              
              scan = new Scan(startRow);
              filter = new PageFilter((long)rows);
              scan.setFilter(filter);
              
              return hbaseTemplate.find(TABLE_NAME, scan, new RowMapper<DataSource>(){
                  @Override
                  public DataSource mapRow(Result result, int i) throws Exception {
                     return mapDataSource(result);
                  }
               });
              
              
          }
         
        return dataSources;
    }
    
    private DataSource mapDataSource(Result result){
        return new DataSource(Bytes.toString(result.getRow()),
                        Bytes.toString(result.getValue(COL_FAM, URL_COL)),
                        DataSource.SourceType.valueOf((
                Bytes.toString(result.getValue(COL_FAM, SOURCE_TYPE_COL))))
                        );
    }
    
   
    @Override
    public Object putToHBaseTest(String text) {
        throw new UnsupportedOperationException("Not supported yet."); 
        //To change body of generated methods, choose Tools | Templates.
    }
    
}
