/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.service;

import java.util.List;
import pl.hbase.dao.IDataSourceDao;
import pl.hbase.dto.DataPageResponse;
import pl.hbase.dto.ItemsPage;
import pl.hbase.model.DataSource;

/**
 *
 * @author dawid
 */
public class DataSourceService implements IDataSourceService{

    private IDataSourceDao dataSourceDao;

    public DataSourceService(IDataSourceDao dataSourceDao) {
        this.dataSourceDao = dataSourceDao;
    }

    @Override
    public DataSource addDataSource(DataSource dataSource) {
        return dataSourceDao.addDataSource(dataSource);
    }

    @Override
    public List<DataSource> findAll() {
        return dataSourceDao.findAll();
    }
    
    @Override
    public DataPageResponse<DataSource> findDataSources (int page, int rows){
        ItemsPage<DataSource> itemsPage = new ItemsPage<DataSource>(page, rows);
        itemsPage.setItems(dataSourceDao.findDataSources(page, rows));
        return new DataPageResponse<DataSource>(itemsPage);
    }

    @Override
    public void deleteDataSource(DataSource dataSource) {
        dataSourceDao.deleteDataSource(dataSource);
    }
    
    
}
