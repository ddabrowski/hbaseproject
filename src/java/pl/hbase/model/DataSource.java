/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.model;

/**
 *
 * @author dawid
 */
public class DataSource  {

    
    private String name;
    private String url;
    private SourceType sourceType;

    public DataSource(String name, String url, SourceType sourceType) {
        this.name = name;
        this.url = url;
        this.sourceType = sourceType;
    }

    public DataSource() {
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
    }

  

    public enum SourceType {

        TXT,
        ZIP
    }
}
