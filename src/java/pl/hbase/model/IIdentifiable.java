/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.model;

import java.io.Serializable;

/**
 *
 * @author dawid
 */
public interface IIdentifiable<E extends IIdentifiable> extends Serializable {
    /**
     *
     * @return
     */
    String getId();
           
    /**
     *
     * @return
     */
    Identifier<E> getIdentifier();
}
