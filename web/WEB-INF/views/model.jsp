<%@page language="Java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/includes.jsp" %>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>HBase </title>
        
        <link rel="stylesheet" type="text/css" href="/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="/resources/jqgrid/css/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" href="/resources/jquery-ui-1.10.2.custom/css/ui-lightness/jquery-ui-1.10.2.custom.css" />
        
        <script type="text/javascript" src="/resources/js/jquery-1.9.1.js"> </script> 
        <script type="text/javascript" src="/resources/jquery-ui-1.10.2.custom/js/jquery-ui-1.10.2.custom.js"> </script> 
        <script type="text/javascript" src="/resources/jqgrid/js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="/resources/jqgrid/js/i18n/grid.locale-pl.js"></script>
        
        <script language="javascript" type="text/javascript" src="/resources/js/jquery.jqplot.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/resources/css/jquery.jqplot.css" />
        
    
    </head>
    
    <body>
		<div id="header-container">
			<div id="header">
				<a href="index.php"><img src="/resources/images/logo.png" /></a>
			</div>
		</div>
		<div id="container">
                    
                    
			 <div id="content">
                             <div id="mainMenu">
                                <c:import url="${mainMenu}"/>
                            </div>
                                     <c:import url="${content}"/>
                         </div>
		</div>
		<div id="footer-container">
			<div id="footer">
				<div id="footer-left">
					<h2 class="green">Autorzy</h2>
					<ul>
						<li class="footer-text">Dawid Dąbrowski</li>
						<li class="footer-text">Grzegorz Boczar</li>
					</ul>
				</div>
				<div id="footer-middle">
					<h2 class="footer-line-height">Big Data</h2>				
				</div>
				<div id="footer-right">
					<a href="http://www.pk.edu.pl/"><img src="/resources/images/pk.png" class="footer-image" /></a>
					<a href="http://www.fmi.pk.edu.pl/"><img src="/resources/images/fmi.png" class="footer-image" /></a>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div id="footer-copyright">
			<p class="footer-text">Copyright &copy; 2013</p>
		</div>
	</body>
</html>


    <body>
        <div id="bigWrapper">
        
          

       
        
        </div>
            
     </body>
</html>
