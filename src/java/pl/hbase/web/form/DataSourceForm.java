/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.web.form;

import org.hibernate.validator.constraints.NotEmpty;
import pl.hbase.model.DataSource;

/**
 *
 * @author dawid
 */
public class DataSourceForm {

    @NotEmpty(message="Pole nie może być puste")
    private String name;
    
    @NotEmpty(message="Pole nie może być puste")
    private String url;
    
  
    private DataSource.SourceType sourceType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public DataSource.SourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(DataSource.SourceType sourceType) {
        this.sourceType = sourceType;
    }
    
    
    
}
