/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.facade;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.hbase.dto.DataPageResponse;
import pl.hbase.model.DataSource;
import pl.hbase.service.IDataSourceService;
import pl.hbase.service.IDataTableService;
import pl.hbase.tools.FileTools;
import static pl.hbase.tools.FileTools.getEntries;
import static pl.hbase.tools.FileTools.getLines;
import pl.hbase.tools.UrlTools;

/**
 *
 * @author dawid
 */
public class Facade implements IFacade {

    private IDataSourceService dataSourceService;
    private IDataTableService dataTableService;

    public Facade(IDataSourceService dataSourceService,
            IDataTableService dataTableService) {
        this.dataSourceService = dataSourceService;
        this.dataTableService = dataTableService;
    }

    
    
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    @Override
    public void updateDataSource(String url, String name, DataSource.SourceType sourceType) {
      
        if(sourceType.equals(DataSource.SourceType.TXT)){
            try {
                File file = UrlTools.downloadFile(url);
                String text = FileTools.getTextFromFile(file);
                        System.out.println(url);
                
                for(String s : getLines(text)){
                          dataTableService.putLine(s, name);
                  
                }
               
                file.delete();
                
                    
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Facade.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(sourceType.equals(DataSource.SourceType.ZIP)){
            File file = UrlTools.downloadFile(url);
            List<File> files = FileTools.unZipFile(file);
            
            for(File f : files){
                try {
                    String text = FileTools.getTextFromFile(f);
                    
                    for(String s : getLines(text)){
                          dataTableService.putLine(s, name);
                  
                    }
                    
                    FileTools.deleteTempFile(f);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Facade.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            FileTools.deleteTempFile(file);
        }
        
        
    }
    
    
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    @Override
    public DataSource addDataSource(DataSource dataSource) {
        return dataSourceService.addDataSource(dataSource);
    }

    @Override
    public List<DataSource> findAll() {
        return dataSourceService.findAll();
    }

    @Override
    public DataPageResponse<DataSource> findDataSources(int page, int rows) {
        return dataSourceService.findDataSources(page, rows);
    }

    @Override
    public void deleteDataSource(DataSource dataSource) {
        dataSourceService.deleteDataSource(dataSource);
    }
}
