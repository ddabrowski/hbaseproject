/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.hbase.dao.IHBaseTemplateDao;
import pl.hbase.tools.FileTools;
import pl.hbase.tools.UrlTools;

/**
 *
 * @author dawid
 */

@Controller
public class HomeController {
    
    private IHBaseTemplateDao hBaseTemplateDao;

    @Autowired
    public HomeController(IHBaseTemplateDao hBaseTemplateDao) {
        this.hBaseTemplateDao = hBaseTemplateDao;
    }
    
    
    
    @RequestMapping("/home.htm")
    public String home(){
        return "homeView";
    }
    
    @RequestMapping("/add.htm")
    public String addToHbase(@RequestParam(value="text")String text){
        hBaseTemplateDao.putToHBaseTest(text);
        return "redirect:/home.htm";
    } 
    /**
     * 
     * @param model
     * @return 
     */
    @RequestMapping("/getData.htm")
    public String downloadTest(Model model, 
                                                    @RequestParam(value="url")String url,
                                                    @RequestParam(value="type", defaultValue = "txt")String type) throws FileNotFoundException{
        
        
        if("txt".equals(type)){
            File file =UrlTools.downloadFile(url);
            model.addAttribute("test", FileTools.getTextFromFile(file));
            file.delete();
        }
        if("zip".equals(type)){
            File file = UrlTools.downloadFile(url);
            List<File> files = FileTools.unZipFile(file);
            String tekst = "<br/>";
            for(File f : files){
                tekst+=FileTools.getTextFromFile(f)+"<br/>Kolejny plik <br/> ";
                FileTools.deleteTempFile(f);
            }
            FileTools.deleteTempFile(file);
            model.addAttribute("test", tekst);
        }
       
        
        return "testView";
    }
    
    
}
