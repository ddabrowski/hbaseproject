/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.facade;


import java.util.List;
import pl.hbase.dto.DataPageResponse;
import pl.hbase.model.DataSource;

/**
 *
 * @author dawid
 */
public interface IFacade {
    

    DataSource addDataSource(final DataSource dataSource);
    
    List<DataSource> findAll();
    
    DataPageResponse<DataSource> findDataSources (int page, int rows);
    
    void deleteDataSource(final DataSource dataSource);
    
    void updateDataSource(String url, String name, DataSource.SourceType sourceType);
    
}
