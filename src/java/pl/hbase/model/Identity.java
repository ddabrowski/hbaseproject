/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.model;

import java.io.Serializable;
import pl.hbase.tools.IdGenerator;

/**
 *
 * @author dawid
 */
public class Identity implements Serializable {

    private static final long serialVersionUID = -851007604693746057L;
    private String id = IdGenerator.id();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
