/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.dao;

import java.util.List;
import pl.hbase.model.DataSource;

/**
 *
 * @author dawid
 */
public interface IDataSourceDao {
    
    DataSource addDataSource(final DataSource dataSource);
    
    List<DataSource> findAll() ;
    
     List<DataSource> findDataSources(int page, int rows);
     
     void deleteDataSource(final DataSource dataSource);
}
