/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.controller;


import java.util.Arrays;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import pl.hbase.facade.IFacade;
import pl.hbase.model.DataSource;
import pl.hbase.web.form.DataSourceForm;
import pl.logic.dto.Pair;

/**
 *
 * @author dawid
 */

@Controller 
@RequestMapping("/user/addDataSource.htm")
@SessionAttributes("dataSourceForm")
public class AddDataSourceController {
    
    @Autowired
    @Qualifier(value = "validator")
    private Validator validator;
    
    private IFacade facade;

    @Autowired
    public AddDataSourceController(IFacade facade) {
        this.facade = facade;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String addDataSource(Model model){
        model.addAttribute("dataSourceForm", new DataSourceForm());
        return "addDataSourceView";
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public String addDataSource(@Valid @ModelAttribute("dataSourceForm")DataSourceForm dataSourceForm,
                                                        BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            return "addDataSourceView";
        }
        else {
            DataSource dataSource = new DataSource();
            dataSource.setName(dataSourceForm.getName());
            dataSource.setUrl(dataSourceForm.getUrl());
            dataSource.setSourceType(dataSourceForm.getSourceType());
            facade.addDataSource(dataSource);
        }
        
        return "redirect:/home.htm";
    }
    
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
    
    public static final List<Pair<String, String>> SOURCE_TYPES = Arrays.asList(
            new Pair<String, String>(DataSource.SourceType.TXT.name(), "Plik txt"),
            new Pair<String, String>(DataSource.SourceType.ZIP.name(), "Plik zip"));
    
    @ModelAttribute("types")
    private List<Pair<String, String>> getTypes(){
        return SOURCE_TYPES;
    }
}
