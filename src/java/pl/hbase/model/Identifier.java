/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.model;
import java.io.Serializable;
import javax.validation.constraints.NotNull;

public class Identifier<E extends IIdentifiable> implements Serializable {

    private static final long serialVersionUID = 4027360895804420366L;

    private String id;

    public Identifier(@NotNull String id) {
        this.id = id;
    }

    private Identifier(@NotNull E identifiable) {
        this.id = identifiable.getId();
    }

    @NotNull
    public String getId() {
        return id;
    }

    @NotNull
    public static <E extends IIdentifiable> Identifier<E> create(@NotNull E identifiable) {
        return new Identifier<E>(identifiable);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @NotNull
    public static <E extends IIdentifiable> Identifier<E> create(Class<E> clazz, @NotNull String id) {
        return new Identifier<E>(id);
    }

    public boolean validate(String id) {
        return this.id.equals(id);
    }

    public <E extends IIdentifiable> boolean validate(Identifier<E> identifier) {
        return this.id.equals(identifier.getId());
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Identifier that = (Identifier) o;
        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

}