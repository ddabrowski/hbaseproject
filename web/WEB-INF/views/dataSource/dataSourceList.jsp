<%@page language="Java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/includes.jsp" %>





<script type="text/javascript">
   

   

    function actions(cellvalue, options, rowObject) {
      return  '<a class="delete warning" href="/dataSources/delete.htm?name=' + cellvalue + '">Usun</a> ';
    }

    function updateAction(cellvalue, options, rowObject) {
        return '<a class="update" href="/dataSources/updateData.htm?name='+cellvalue+'&url='+rowObject.url+'&type='+rowObject.sourceType+'">Pobierz dane</a>';
        
    }
    
    function details(cellvalue, options, rowObject) {
        return '<a  href="/dataSources/showDetails.htm?name='+cellvalue+ '">Wartości</a> ';
        
    }
    
    function chart(cellvalue, options, rowObject) {
        return '<a  href="/dataSources/showChart.htm?name='+cellvalue+ '">Wykres</a> ';
        
    }
    
    
    function initButtons() {
        $("a.delete").click(function (event) {
            if (!confirm('Chcesz trwale usunąć wpis?')) {
                event.preventDefault();
            }
        });

        $("a.update").click(function (event) {
            event.preventDefault();
            if (confirm('Chcesz uaktualnić te dane?')) {
                var location = $(this).attr('href');
                if (location != "#")
                    $.ajax({
                        type:'GET',
                        url:location,
                        cache:false,
                        timeout:240000,
                        success:function (data) {
                        },
                        error:function (data) {
                        }
                    });

                return true;
            }
        });
    }
    $(document).ready(function () {
        initButtons();

        $("#delDataSource-dialog-message").dialog({
            autoOpen:false,
            buttons:{
                Delete:function () {
                    var id = $(this).data('id');
                    $.ajax({
                        type:'POST',
                        url:'<c:url value="/admin/deleteDataSource.htm"/>',
                        data:{id:id},
                        cache:false,
                        timeout:240000,
                        success:function (data) {
                            //                            alert("data: "+data);
                            $("#dataSources").trigger('reloadGrid');
                        },
                        error:function () {
                            alert("Error while deleting date source.");
                        }
                    });
                    $(this).dialog("close");
                },
                Cancel:function () {
                    $(this).dialog("close");
                }
            }
        });


        $("#dataSources").jqGrid({
            autowidth:true,
            height:500,
            url:'/dataSources/page.htm',
            datatype:"json",
            mtype:"GET",
              colNames:['Nazwa', 'URL', 'Typ' , 'Akcje','Aktualizacja','Detale','Wykresy'],
            colModel:[
                {name:'name', index:'name', width:150, editable:false},
                {name:'url', index:'url', width:300, editable:false},
                {name:'sourceType', index:'sourceType', width:45, editable:false, editrules:{required:false}, editoptions:{size:42}},
                {name:'name', index:'name', width:50, editable:false, formatter:actions},
                {name:'name', index:'name', width:50, editable:false, formatter:updateAction},
                {name:'name', index:'name', width:50, editable:false, formatter:details},
                {name:'name', index:'name', width:50, editable:false, formatter:chart}
       
            ],
            multiselect:false,


            rowNum:30,
            rowList:[10, 20, 30, 50, 100, 1000],
            pager:'#dataSourcesPager',
            toppager:true,
            rownumbers:true,
            viewrecords:true,
            caption:"",
            gridComplete:function (id) {
                $(".delDataSource").bind('click', function (event) {
                    event.preventDefault();
                    var id = $(this).attr('href');
                    $("#delDataSource-dialog-message").data('id', id).dialog('open');
                });

               

                $(".button,.smallButton,.tinyButton").button();
                initButtons();
            },

            jsonReader:{
                root:"rows",
                page:"page",
                total:"total",
                records:"records",
                repeatitems:false,
                userdata:"userdata"
            }
        });

        $("#dataSources").jqGrid('navGrid', '#dataSourcesPager', {search:false, edit:false, add:false, del:false});
    });
</script>


<br/>
<br/>
<%--@elvariable id="dataSources" type="java.util.List<pl.benhauer.inwestor.model.data.DataSource>"--%>
<table id="dataSources"></table>
<div id="dataSourcesPager"></div>

<div class="hidden">
    <div id="delDataSource-dialog-message" title="Data Source has been deleted">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
          delete
        </p>
    </div>
</div>