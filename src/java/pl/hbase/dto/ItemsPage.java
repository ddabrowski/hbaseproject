/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.dto;

/**
 *
 * @author dawid
 */
import java.util.List;

public class ItemsPage<E> {

    protected int page = 1;
    //    private long itemsSize;
    private List<E> items;
    private int pageSize;

    public ItemsPage(int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    public List<E> getItems() {
        return items;
    }

    public void setItems(List<E> items) {
        this.items = items;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public long getItemsSize() {
        return items.size() >= pageSize ? page * pageSize + 1 : (page - 1) * pageSize + items.size();
    }

    public void setItemsSize(long itemsSize) {
//        this.itemsSize = itemsSize;
//        if (page >= getPages())
//            page = getPages() - 1;
    }

    public int getPages() {
//        int ct = (int) (itemsSize / pageSize);
//        int rt = (int) (itemsSize % pageSize);
//        return rt == 0 ? ct : ct + 1;
        return items.size() < pageSize ? page : page + 1;
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }


}
