/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dawid
 */
public class DataPageResponse <T> implements Serializable{
    
    
    private String page = "1";
    private String total = "0";
    private String records = "0";

    private List<T> rows = new ArrayList<T>();

    public DataPageResponse() {
    }
    
    
    
      public DataPageResponse(ItemsPage<T> itemsPage) {
       this.page = String.valueOf(itemsPage.getPage());
       this.records = String.valueOf(itemsPage.getItemsSize());
       this.rows = itemsPage.getItems();
       this.total = String.valueOf(itemsPage.getPages());
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }
   
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DataPageResponse");
        sb.append("{page='").append(page).append('\'');
        sb.append(", total='").append(total).append('\'');
        sb.append(", records='").append(records).append('\'');
        sb.append(", rows=").append(rows);
        sb.append('}');
        return sb.toString();
    }

}
