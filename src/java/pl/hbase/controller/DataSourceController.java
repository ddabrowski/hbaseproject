/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.hbase.dao.IDataSourceDao;
import pl.hbase.dto.DataPageResponse;
import pl.hbase.facade.IFacade;
import pl.hbase.model.DataSource;
import pl.logic.dto.Pair;

/**
 *
 * @author dawid
 */

@Controller
public class DataSourceController {
    
    private IFacade facade;
    private IDataSourceDao dao;
    private static final Logger LOG = Logger.getLogger(DataSourceController.class.getName());
    
    @Autowired
    public DataSourceController(IFacade facade, IDataSourceDao dao) {
        this.facade = facade;
        this.dao = dao;
    }
    
    
    @RequestMapping("/dataSources/list.htm")
    public String findAllDataSources(Model model){
        model.addAttribute("dataSources", facade.findAll());
        return "sourcesListView";
    }
    
    
    @RequestMapping("/dataSources/page.htm")
    public 
            @ResponseBody
            DataPageResponse<DataSource> list (@RequestParam(value="page", required= false, defaultValue = "1")int page,
                                                   @RequestParam(value="rows", required= false, defaultValue = "10")int rows){
        
            return facade.findDataSources(page, rows);
    }
    
    
    
    
    @RequestMapping("/dataSources/delete.htm")
    public String deleteDataSource(@RequestParam(value="name")String name){
        DataSource dataSource = new DataSource();
        dataSource.setName(name);
        facade.deleteDataSource(dataSource);
        LOG.info("usuwam o nazwie"+dataSource.getName());
        return "redirect:/dataSources/list.htm";
    }
    
    
}
