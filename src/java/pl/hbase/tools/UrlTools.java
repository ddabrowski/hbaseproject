/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.tools;

/**
 *
 * @author dawid
 */
import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import org.springframework.security.crypto.codec.Base64;

public class UrlTools {

    public static final String HTTP_PREFIX = "http://";
    public static final String HTTPS_PREFIX = "https://";

    public static final int DEFAULT_CONNECTION_TIMEOUT = 60000;
    public static final int DEFAULT_READ_TIMEOUT = 5 * 60000;

    


    public static String validUrl(String url) {
        if (url == null)
            return null;
        return hasHttpPrefix(url) ? HTTP_PREFIX + url : url;
    }

    public static boolean hasHttpPrefix(String landingUrl) {
        return !landingUrl.startsWith(HTTP_PREFIX) && !landingUrl.startsWith(HTTPS_PREFIX);
    }

    public static String getURL(String sourceURL) {
        try {
            StringBuilder sb = new StringBuilder();
            URL url = new URL(sourceURL);

            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            authorise(sourceURL, uc);
            uc.setRequestProperty("Cache-Control", "no-cache");
            uc.addRequestProperty("Cache-Control", "max-age=0");
            uc.setRequestProperty("Pragma", "no-cache");

            uc.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);
            uc.setReadTimeout(DEFAULT_READ_TIMEOUT);
            uc.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null)
                sb.append(line);

            reader.close();

            uc.disconnect();
            return sb.toString();
        } catch (MalformedURLException e) {

            return null;
        } catch (IOException e) {

            return null;
        }
    }

    public static String getUntrustedURL(String sourceURL) {
        if (sourceURL.startsWith("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts;
            trustAllCerts = new TrustManager[]{
     new X509TrustManager() {
         @Override
         public java.security.cert.X509Certificate[] getAcceptedIssuers() {
             return null;
         }

         @Override
         public void checkClientTrusted(
                 java.security.cert.X509Certificate[] certs, String authType) {
         }

         @Override
         public void checkServerTrusted(
                 java.security.cert.X509Certificate[] certs, String authType) {
         }
     }
};

            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception ignored) {
            }
        }

        try {
            StringBuilder sb = new StringBuilder();
            URL url = new URL(sourceURL);

            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            if (sourceURL.startsWith("https"))
                ((HttpsURLConnection) uc).setHostnameVerifier(new CustomizedHostnameVerifier());

            authorise(sourceURL, uc);
            uc.setRequestProperty("Cache-Control", "no-cache");
            uc.addRequestProperty("Cache-Control", "max-age=0");
            uc.setRequestProperty("Pragma", "no-cache");

            uc.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);
            uc.setReadTimeout(DEFAULT_READ_TIMEOUT);
            uc.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null)
                sb.append(line);

            reader.close();

            uc.disconnect();
            return sb.toString();
        } catch (MalformedURLException e) {

            return null;
        } catch (IOException e) {

            return null;
        }
    }


    public static String postURL(String sourceURL, String body) {
        PrintWriter pw = null;
        try {
            URL url = new URL(sourceURL);
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            authorise(sourceURL, uc);
            uc.setDoOutput(true);
            uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            uc.setRequestProperty("Cache-Control", "no-cache");
            uc.addRequestProperty("Cache-Control", "max-age=0");
            uc.setRequestProperty("Pragma", "no-cache");

//                for (Pair<String, String> header : headers)
//                    uc.setRequestProperty(header.getObj1(), header.getObj2());

            uc.connect();

            pw = new PrintWriter(uc.getOutputStream());
            pw.println(body);
            pw.close();

            BufferedReader reader;
            try {
                reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            } catch (IOException e) {
                reader = new BufferedReader(new InputStreamReader(uc.getErrorStream()));
            }
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null)
                sb.append(line);

            reader.close();

            uc.disconnect();
            return sb.toString();
        } catch (MalformedURLException e) {
            return null;
        } catch (IOException e) {
            return null;
        } finally {
            safeClose(pw);
        }
    }

    private static void safeClose(PrintWriter reader) {
            if (reader != null)
             reader.close();
    }    
    
    public static File downloadFile(String sourceUrl) {
        //log.info("About to download {0}", sourceUrl);
        File tempFile = null;

        try {
            URL url = new URL(sourceUrl);
            URLConnection conn = url.openConnection();
            authorise(sourceUrl, conn);

            InputStream in = conn.getInputStream();

            tempFile = File.createTempFile("inw_", getFileExt(sourceUrl));

            FileOutputStream out = new FileOutputStream(tempFile);
            byte[] b = new byte[1024];
            int count;
            while ((count = in.read(b)) >= 0)
                out.write(b, 0, count);

            out.flush();
            out.close();
            in.close();

          //  log.info("Downloaded {0} to {1}", sourceUrl, tempFile.getAbsolutePath());

        } catch (IOException e) {
            //log.error(e, "Cannot download file");
        }

        return tempFile;
    }

    public static File downloadUntrustedFile(String sourceUrl) {
        if (sourceUrl.startsWith("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        @Override
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception ignored) {
            }
        }

        //log.info("About to download {0}", sourceUrl);
        File tempFile = null;

        try {
            URL url = new URL(sourceUrl);
            URLConnection conn = url.openConnection();
            if (sourceUrl.startsWith("https"))
                ((HttpsURLConnection) conn).setHostnameVerifier(new CustomizedHostnameVerifier());
            authorise(sourceUrl, conn);

            InputStream in = conn.getInputStream();

            tempFile = File.createTempFile("inw_", getFileExt(sourceUrl));

            FileOutputStream out = new FileOutputStream(tempFile);
            byte[] b = new byte[1024];
            int count;
            while ((count = in.read(b)) >= 0)
                out.write(b, 0, count);

            out.flush();
            out.close();
            in.close();

          //  log.info("Downloaded {0} to {1}", sourceUrl, tempFile.getAbsolutePath());

        } catch (IOException e) {
            // log.error(e, "Cannot download file");
        }

        return tempFile;
    }

    private static void authorise(String sourceUrl, URLConnection conn) {
        if (sourceUrl.contains("@")) {
            String userpass = sourceUrl.replace("http://", "");
            userpass = userpass.replace("https://", "");
            userpass = userpass.split("\\@")[0];
            String basicAuth = "Basic " + new String(Base64.encode(userpass.getBytes()));
            conn.setRequestProperty("Authorization", basicAuth);
        }
    }

    private static String getFileExt(String sourceUrl) {
        String extension = "";

        int i = sourceUrl.lastIndexOf('.');
        int p = Math.max(sourceUrl.lastIndexOf('/'), sourceUrl.lastIndexOf('\\'));

        if (i > p)
            extension = sourceUrl.substring(i + 1);

        return extension;
    }


    public static void main(String[] args) throws UnsupportedEncodingException {
        String fbUserId = "1496696049";
        String fbOAuth = "AAADjhtZBC54IBAHlOxc0ACN7K05YG10ZAXV9hpHouPiYvfWZAZA27ESBRO9iP5voy7Pf7CPYmMkxqmhZAjBcZBJuU6ANEfY1bQwDuDCY6s5afYfYRMKdqE";

        
        String result = "";
    }

    private static class CustomizedHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}
