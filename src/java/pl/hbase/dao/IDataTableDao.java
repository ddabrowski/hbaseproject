/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.hbase.dao;

import java.util.List;
import pl.hbase.dto.DataPageResponse;
import pl.hbase.model.DataSourceDetails;

/**
 *
 * @author dawid
 */
public interface IDataTableDao {
  
    void addDataTable(final String line, final String name);
    
    List<DataSourceDetails> findData(String name, int page, int rows);
    
    DataPageResponse<DataSourceDetails> findDataPages (String name, int page, int rows);
    
    public List<DataSourceDetails> findData(String name);
    
}
